<?php
/**
 * Template Name: Player
 *
 * @package Fictive
 */

get_header(); ?>
	<script>
	  audiojs.events.ready(function() {
	    var as = audiojs.createAll();
	  });
	</script>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<div class="hentry-wrapper">
			<article id="post-5" class="post-5 page type-page status-publish hentry">
				<header class="entry-header">
					<h4 class="entry-title">Escuchar en vivo</h4>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<audio src="http://www.uajms.edu.bo/radio925" preload="auto" />
				</div>
										<!-- .entry-content -->
				<div class="entry-meta">		
				</div>
			</article><!-- #post-## -->
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
